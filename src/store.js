export function createStore(reducer) {
  var state = undefined
  var listeners = []
  return {
    dispatch(action) {
      console.log(action.type, action.payload)
      console.log('state before:', state)
      state = reducer(state, action)
      console.log('state after:', state)
      listeners.forEach(listener => listener())
    },
    getState() {
      return state
    },
    subscribe(listener) {
      listeners.push(listener)
    }
  }
}
export default createStore
import React from 'react'
import ReactDOM from 'react-dom'
import App from './components/App.jsx'
import getPosts from './api/getPosts.js'
import getUsers from './api/getUsers.js'
import reducer from './reducer.js'
import { applyMiddleware, createStore } from 'redux'
import { Provider } from 'react-redux'
import createLogger from 'redux-logger'

var fullPosts = Promise.all([getPosts(), getUsers()])
.then(([posts, users]) =>
  posts.map(post => ({
    id: post.id,
    name: users[post.userId],
    text: post.body,
    userId: post.userId
  }))
)

const store = createStore(reducer,
  applyMiddleware(createLogger())
)

function setPosts(posts) {
  store.dispatch({
    type: 'SET_POSTS',
    payload: posts
  })
}
function setFilter(filter) {
  store.dispatch({
    type: 'SET_FILTER',
    payload: filter
  })
}

fullPosts.then(setPosts)

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('yo')
)

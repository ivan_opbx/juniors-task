import { combineReducers } from 'redux'
import filter from './reducers/filter'
import posts from './reducers/posts'

export default combineReducers({
  filter: filter,
  posts: posts
})

export default function getUsers() {
  return fetch('//dev.onlinepbx.ru/users.php')
    .then(response => response.json())
    .then(
      response => response.reduce(
        (users, user) => Object.assign({}, users, {
          [user.id]: user.name
        }),
        {}
      )
    )
}
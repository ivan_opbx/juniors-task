import React from 'react'

export function Post({ id, userId, name, text }) {
  return (
    <div>
      <p>
        <small>{id}</small>
        {' '}
        {name}
      </p>
      <p>{text}</p>
    </div>
  )
}

export default Post

import React from 'react'
import { connect } from 'react-redux'

export class Filter extends React.Component {
  constructor() {
    super()
    this.handleSubmit = this.handleSubmit.bind(this)
  }
  handleSubmit(event) {
    event.preventDefault()
    this.props.setFilter(this.input.value)
  }
  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <input
          type="number"
          onChange={this.handleSubmit}
          ref={node => this.input = node}
        />
        <button type="button" onClick={this.handleSubmit}>Filter</button>
      </form>
    )
  }
}

function mapStateToProps(state) {
  return {
    filter: state.filter
  }
}
function mapDispatchToProps(dispatch) {
  return {
    setFilter: function(newFilter) {
      dispatch({
        type: 'SET_FILTER',
        payload: newFilter
      })
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Filter)

import React from 'react'
import { connect } from 'react-redux'
import Filter from './Filter.jsx'
import Posts from './Posts.jsx'

export function App() {
  return (
    <div>
      <Filter />
      <Posts />
    </div>
  )
}

export default App

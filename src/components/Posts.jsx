import React from 'react'
import { connect } from 'react-redux'
import { Post } from './Post.jsx'

export function Posts({ posts }) {
  if (!posts || posts.length == 0) {
    return <div>Nothing found</div>
  }
  return (
    <div>
      {
        posts.map(
          post => <Post key={post.id} {...post} />
        )
      }
    </div>
  )
}

function mapStateToProps(state) {
  const filter = state.filter
  const posts = Number(filter)
      ? state.posts.filter(post => post.userId == filter)
      : state.posts
  return {
    posts
  }
}

export default connect(mapStateToProps)(Posts)

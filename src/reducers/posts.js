export default function reducer(state = [], action = {}) {
  if (action.type == 'SET_POSTS') {
    return action.payload
  }
  return state
}
export default function reducer(state = 0, action = {}) {
  if (action.type == 'SET_FILTER') {
    return action.payload
  }
  return state
}